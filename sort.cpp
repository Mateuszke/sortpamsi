#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <time.h>
#include <iomanip>
#include <chrono>
#include "film.hh"
#define MAX 1010292
#define ILOSC 10000
using namespace std;

void shellSort(Film *tablica, int n){ //SHELL sort z odstępem początkowym równym połowie tablicy, i pomniejszanego 2krotnie
    // zaczynamy z przerwą równą połowie ilości elementów, potem zmniejszamy o połowę z każdym przejściem
    //przestawiamy wszystkie elementy znajdujące się od siebie w odleglosci gap
    for (int gap=n/2;gap>0;gap/=2)
    {
        
        //porównujemy elementy oddalone od siebie o gap tyle razy ile bylo podzbiorów
        for (int i = gap; i < n; i++){
            // zapisujemy pierwszy do zmiennej pomocniczej/tymczasowej
            Film temp = tablica[i];
  
            
            //porównujemy az do momentu znalezienia odpowiedniego miesjsca dla tab[i] w podtablicach
            int j;            
            for (j = i; j >= gap && tablica[j-gap]<temp;j-=gap){  //nie chcemy zmieniac i!
                tablica[j] = tablica[j-gap];
            }
            //  wstawiamy oryginalnie poczatkowy element w dobrym miejscu, jeśli trzebabyło zamienić, to zamiana
            tablica[j] = temp;
        }
    }
}






//funkcja budująca kopiec i sprawdzająca warunki kopca
void heapify(Film *tablica, int n, int i) 
{
    int najwiekszy=i; //indeks korzenia
    int l=2*i+1; //indeks lewego dziecka
    int r=2*i+2; //indeks  prawego dziecka

    // jesli lewy jest mniejszy od roota
    if (l<n && tablica[l]<tablica[najwiekszy])
        najwiekszy=l;
 
    // jesli prawy jest mniejszy niz najmniejszy do tej pory
    if (r<n && tablica[r]<tablica[najwiekszy])
        najwiekszy=r;
 
    // jesli najwiekszy nie jest korzeniem
    if (najwiekszy!=i) {
        swap(tablica[i],tablica[najwiekszy]);
 
        
        heapify(tablica,n,najwiekszy);
    }
}

//funkcja sortująca tablicę stosując zasadę kopca
void heapSort(Film *tablica, int n)
{
    // zbuduj kopiec
    for (int i=n/2-1;i>=0;i--){
        heapify(tablica, n, i);
    }
    
    for (int i = n - 1; i > 0; i--) {
        //najw na koniec
        swap(tablica[0], tablica[i]);
 
        // zeby miec znowu najmn na gorze
        heapify(tablica, i, 0);
    }
}






template<typename typ>
void merge(typ *tablica,int pierwszy,int mid,int ostatni){ //dwie tablice od pierwszego-mid(do srodkowego) i od mid+1 do ostatniego

    int r1=mid-pierwszy+1; //rozmiary tablic tymczasowych
    int r2=ostatni-mid;
    //typ L[r1],P[r2];
    typ *L=new typ[r1];
    typ *P=new typ[r2];
    for(int i=0;i<r1;i++){     //kopiowanie danych do tablic tymczasowych
        L[i]=tablica[pierwszy+i];

    }
    for(int j=0;j<r2;j++){
        P[j]=tablica[mid+1+j];
    }
    int i=0;// nowe indeksy dla 1.i 2. podtablicy
    int j=0;
    int k=pierwszy;

    while(i<r1 && j<r2){ // dopóki nie dojdziemy do konca jednej z  podtablic
        if(L[i]>P[j]){ //wybieraj wiekszy z 2 i dodaj na poczatek scalonej
            tablica[k]=L[i]; //tablice przed scalaniem sa juz posortowane
            i++;
        }
        else{
            tablica[k]=P[j];
            j++;
        }
        k++;

    }

    while(i<r1){ //jak dojdziemy do konca jednej, to te ktore zostaly dodajemy na koniec
        tablica[k]=L[i];
        i++;k++;
    }
    while(j<r2){
        tablica[k]=P[j];
        j++;k++;
    }
    delete[] L; //usuwam tymczasowe
    delete[] P;
}
template<typename typ>
void MergeSort(typ *tablica,int pierwszy, int ostatni){
    //cout<<"wszedlem do mergesorta"<<endl;
    if(pierwszy>=ostatni){
        return;
    }
//dziel dopoki bedzie rozmiar 1, dopoki pierwszy!=ostatni
    int mid=pierwszy+(ostatni-pierwszy)/2; //punkt podzialu
    
    MergeSort(tablica,pierwszy,mid);

    MergeSort(tablica,mid+1,ostatni);
 //sortuj te mniejsze i scalaj az nie scali sie 
    merge(tablica,pierwszy,mid,ostatni);

}







//funkcja przeszukująca bazę w poszukiwaniu elementów bez oceny i usuwająca je
int liczbabezoceny=0;
Film* Filtruj(Film *TablicaNieFiltr){

    int przesun=0;

    //auto start1=chrono::high_resolution_clock::now();

    for(int f=0;f<ILOSC;f++){
        //cout<<TablicaNieFiltr[f].numer<<endl;
        if(TablicaNieFiltr[f].ocena==0){  //przeszukuje cala tablcie w poszukiwaniu braku oceny
            liczbabezoceny+=1;
            }
    }

    // auto stop1=chrono::high_resolution_clock::now();
    // double duration1=chrono::duration_cast<chrono::microseconds>(stop1-start1).count();
    // duration1*=1e-6;
    // cout<<"Czas przeszukiwania: "<<ILOSC<<" elementow: "<<fixed<<duration1<<setprecision(6)<<" sekund"<<endl;

    Film* TablicaFiltr=new Film[ILOSC-liczbabezoceny]; //nowa tablica na tylko ocenione filmy
    cout<<"tylebyło:"<<liczbabezoceny<<endl;
    int tylezostalo=liczbabezoceny;
    for(int d=0;d<ILOSC;d++){ //gdy bedzie film bez oceny, to inkrementuj przesuniecie i odejmij od wartosci pokazujacej ile jeszce zostalo wyszukanych
        if(TablicaNieFiltr[d].ocena==0){ //i nie przepisuj
            przesun=przesun+1;
            tylezostalo-=1;
        }
        else{ //jesli w porzadku to przepisz na kolejne miejsce, bez przerwy, bo d-przesun
            TablicaFiltr[d-przesun]=TablicaNieFiltr[d];
            //cout<<TablicaFiltr[d-przesun].numer<<endl;
        }
    }
    //cout<<endl<<endl;
    // for(int i=0;i<ILOSC-liczbabezoceny;i++){
    //     //cout<<TablicaFiltr[i].numer<<endl;
    // }
    return TablicaFiltr; //zwracam tą zfiltrowaną tablcie

}

//funkcja pozwalająca sprawdzić, który z 2 tytułów powiniein być pierwszy w kolejności alfabetycznej
// jeśli znak a>b, to a jest później w alfabecie
bool SprawdzAlfabetycznie(string a, string b){ 
        if(a==b){
            return true;
        }
        int o=0,p=0;
        if(a[o]=='"'){//pomiń cudzysłowia w obydwu, jeśli występują
            o++;
        }
        if(b[p]=='"'){
            p++;
        }
        while(a[o]==b[p]){ //dopóki znaki się sobie równają, to przechodz do kolejnego aby porównać
            //cout<<a[o]<<' '<<b[p]<<endl;
            o++;p++;
        }
        if(a[o]>b[p]){ // jeśli znak ma większą wartość, to zwracaj false, bo jest pozniej w alfabecie
            //cout<<"nie jest przed w alfabetycznym porzadku"<<endl;
            return false;
        }
        else{//jesli jest mniejsze lub rowne to zwracaj true, bo moze byc wczesniej
            //cout<<"tak, jest wieksze w porzadku alfabetycznym"<<endl;
            return true;
        }
    
}


//funkcja pozwalająca zapisać tablicę do pliku o danej nazwie
void ZapiszDoPliku(string NazwaPliku,Film Tablica[]){
    int v=0;
    ofstream Plik(NazwaPliku);
    for(v=0;v<ILOSC-liczbabezoceny;v++){
        Tablica[v].numer=v+1;
        Plik<<Tablica[v];
    }
}

//funkcja wczytująća  listę filmów z bazy danych, o zadanej nazwie i konkretnej ilosci linii
Film* Pobierzfilm(string nazwa,int ilosc){
    //int g=0;
    fstream plik;
    string linia;
    plik.open(nazwa);
    if(plik.good()!=true){//sprawdz, czy udalo sie otworzyc plik
        cout<<"!!! Niepoprawna nazwa pliku, sprawdz pisownie !!! "<<endl<<endl;
        exit(1);
    }
    char znak;

    //int wyjatek=0;
    Film* tablicaFilmow=new Film[ilosc]; //nowa tablica dyn.
    // char *pierwsze=new char[10];
    //     char *drugie=new char[200];
    //     char *trzecie=new char[10];
    //for(int m=0;m<ilosc;m++){
    for(int g=0;g<ilosc;g++){
        int t=0,przecinki=0; //zmienne potrzebne do sprawdzania przecinka w tutule, wiemy ze oryginalnie powinny byc 2
        char pierwsze[10]={0};
        char drugie[200]={0};
        char trzecie[10]={0};
        // char *pierwsze=new char[10];
        // char *drugie=new char[200];
        // char *trzecie=new char[10];
        int i=0,j=0,k=0;
        Film film;
        getline(plik,linia);
        while(linia[t]!='\0'){ //sprawdz ile jest przcinkow w linii
            if(linia[t]==','){
                przecinki+=1;
            }
            t++;
        }
        while(linia[i]!=','){ //wczytaj numer filmu znak po znaku, az do napotkania przecinka
            pierwsze[i]=linia[i];
            i++;
        }
        //cout<<pierwsze<<endl;
        i+=1;
        przecinki--; //jeden juz byl
        while(przecinki>0){ //wczytuj znak po znaku tytul, az do wyzerowania przecinków
            drugie[j]=linia[i];
            i++;j++;
            if(linia[i]==',')
            {
                przecinki--;
            }
        }

        i+=1;


        while(linia[i]!='\0'){ //wczytuj ocene az do konca linii
            trzecie[k]=linia[i];
            k++;i++;
        }

        //cout<<trzecie<<endl;
        // int p=atoi(pierwsze);
        // string tytul=drugie;
        // float o=atof(trzecie);
        film.numer=atoi(pierwsze); //przypisywanie wartosci do filmu
        film.tytul=drugie;
        film.ocena=atof(trzecie);
        //cout<<film.tytul<<endl;
        tablicaFilmow[g]=film; //wstaw film z tymi parametrami do nowej tablicy
        
    }
    plik.close(); //zamykam plik
    // delete[] pierwsze;
    //     delete[] drugie;
    //     delete[] trzecie;
    return tablicaFilmow; //zwracam tablice z filmami
}


int main(){
    cout<<"zaczalem"<<endl;

    Film *TablicaNieFiltr=Pobierzfilm("proj.csv",ILOSC);
    
    cout<<"pobralem"<<endl;
    ZapiszDoPliku("niefiltrowane.txt",TablicaNieFiltr);
    
    
    //********* FILTROWANIE ********
    auto start1=chrono::high_resolution_clock::now();
    
    Film* TablicaFiltr=Filtruj(TablicaNieFiltr);
    
    auto stop1=chrono::high_resolution_clock::now();
    double duration1=chrono::duration_cast<chrono::microseconds>(stop1-start1).count();
    duration1*=1e-6;
    cout<<"Czas filtrowania "<<ILOSC<<" elementow: "<<fixed<<duration1<<setprecision(6)<<" sekund"<<endl;
    
    ZapiszDoPliku("pofiltrowane.csv",TablicaFiltr);
    //*******************************
    
    
    //*********  SORTOWANIE  ******
    auto start=chrono::high_resolution_clock::now();


    //MergeSort(TablicaFiltr,0,ILOSC-liczbabezoceny-1);
    //heapSort(TablicaFiltr,ILOSC-liczbabezoceny);
    shellSort(TablicaFiltr,ILOSC-liczbabezoceny);
    
    
    auto stop=chrono::high_resolution_clock::now();

    double duration=chrono::duration_cast<chrono::microseconds>(stop-start).count();
    duration*=1e-6;
    cout<<"Czas sortowania: "<<fixed<<duration<<setprecision(6)<<" sekund"<<endl;
    


    ZapiszDoPliku("posortowane.csv",TablicaFiltr);
    cout<<"Zapisalem posortowane"<<endl;




    delete[] TablicaNieFiltr;
    delete[] TablicaFiltr;
    return 0;
}
